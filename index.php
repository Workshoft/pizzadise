<head>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="index.css">
</head>

<?php require "nav.php"; ?>

<form method="post" action="receipt.php">
    <table>
	<input type="hidden" name="order">
	<tr><td>Name:</td><td><input type="text" name="name" required></td><br/></tr>
	<tr><td>Surname:</td><td><input type="text" name="surname" required></td><br/></tr>
	<tr><td>Address:</td><td><input type="text" name="address" required></td><br/></tr>
	<tr><td>Phone number:</td><td><input type="text" name="phone-number" required></td><br/></tr>
	<tr><td>Pizza type:</td>
	    <td><select name="pizza-type" required>
		<option value="pepperoni">Pepperoni</option>
		<option value="muzzarella">Muzzarella</option>
		<option value="margherita">Margherita</option>
		<option value="neapolitan">Neapolitan</option>
		<option value="chicago">Chicago</option>
		<option value="sicilian">Sicilian</option>
	    </select>
	    </td>
    </table>
    <button type="submit">Order</button>
</form>


  




